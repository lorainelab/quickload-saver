# About

This IGB App creates an IGB QuickLoad from the custom genome data in IGB.

Use this app to:

* Save custom genomes as IGB QuickLoads so you can easily return to your data after closing IGB.

# How to install this IGB App

## Option 1: Build it on your local computer.

* Clone the repository
* Build the App by running `mvn package`
* Start IGB 9.1 or higher
* Select **Open App Manager** from the IGB Tools menu
* Click **Manage Repositories** button
* Use the file chooser to select the "target" directory in the cloned repository as a new App repository
* Return to IGB App Manager. You should now see a new App named **Save Custom Genome** in the list of available Apps.
* Click **Install** to install the App

## Option 2: Use this repository's Downloads folder as an IGB App repository

Because this repository's **Downloads** folder contains an OBR index file (repository.xml), you can try out the App using the **Downloads** folder as an App Repository.

To do this, follow the instructions above, but instead of selecting a local folder, enter the URL of the **Downloads** folder.

## Option 3: Use IGB App Store

Go to https://apps.bioviz.org. Select the "Hello World App" and then click "Install".

# How to run this IGB App

* Install the app.
* Load a custom genome by choosing File->Open Genome from File.. or click on the DNA icon from the Toolbar.
* Click on Load sequence to view the genome sequence.
* Under the File menu, you should be seeing an option to save custom genome. Go ahead and save.

### Credits

Noor Zahara developed the Quickload Saver App. Nowlan Freese provided technical, scientific, and design advice. Rachel Weidenhammer conducted testing of the app. 